#!/bin/bash
#
# Install Docker.
# Based on this guide: https://docs.docker.com/engine/install/debian/

cat << EOF
*************************
* Docker Installation   *
*************************
EOF

export DEBIAN_FRONTEND=noninteractive

apt remove -y docker.io \
        docker-doc \
        docker-compose \
        podman-docker \
        containerd \
        runc

apt install -y curl \
        ca-certificates \
        gnupg

install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null

# Refreshing new repositories
apt-get update

# Installing docker
apt install -y docker-ce \
        docker-ce-cli \
        containerd.io \
        docker-buildx-plugin \
        docker-compose-plugin
