#!/bin/bash
#
# Initials steps,
# - updating OS

cat << EOF
***************
* Init Script *
***************
EOF

export DEBIAN_FRONTEND=noninteractive

apt update && apt dist-upgrade -y
