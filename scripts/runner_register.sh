#!/bin/bash
#
# Registering the Runner

cat << EOF
**************************
* Gitlab Runner Register *
**************************
EOF

if [ "${GITLAB_RUNNER_TYPE}" == 'shell' ]; then
    # Runner type is shell
    gitlab-runner register \
      --non-interactive \
      --executor "${GITLAB_RUNNER_TYPE}" \
      --description "vagrant-runner ${GITLAB_RUNNER_TYPE}" \
      --url "${GITLAB_URL}" \
      --token "${GITLAB_TOKEN}"
elif [ "${GITLAB_RUNNER_TYPE}" == 'docker' ]; then
    # Runner type is DIND (privileged docker)
    gitlab-runner register \
      --non-interactive \
      --executor "${GITLAB_RUNNER_TYPE}" \
      --docker-image "docker:24.0.5-dind" \
      --docker-privileged \
      --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
      --description "vagrant-runner ${GITLAB_RUNNER_TYPE}" \
      --url "${GITLAB_URL}" \
      --token "${GITLAB_TOKEN}"
fi
