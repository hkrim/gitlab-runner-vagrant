#!/bin/bash
#
# Installing extra/required packages to be used later on

cat << EOF
**********
* Extras *
**********
EOF

export DEBIAN_FRONTEND=noninteractive

# Installing Trivy (image scanning)
# URL: https://aquasecurity.github.io/trivy/v0.50/getting-started/installation/
apt install wget apt-transport-https gnupg lsb-release
wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | gpg --dearmor | tee /usr/share/keyrings/trivy.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/trivy.gpg] https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main" | tee -a /etc/apt/sources.list.d/trivy.list
apt update
apt install trivy
