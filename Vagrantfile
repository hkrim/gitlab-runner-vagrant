# -*- mode: ruby -*-
# vi: set ft=ruby :

# Variables and secrets
require 'yaml'
params = YAML.load_file 'variables.yaml'
secrets = YAML.load_file 'secrets.yaml'

servers=[
    {
        # Runner
        :hostname => params['runner']['hostname'],
        :ip => params['runner']['ip'],
        :box => params['global']['box_name'],
        :ram => params['runner']['memory'],
        :cpu => params['runner']['cores'],
        :disk_size => params['runner']['root_disk']
    }
]

Vagrant.configure("2") do |config|
    # Have to disable 'rsync', as it tries to install packages
    # and fail before the MTU was fixed for my local issue.
    config.nfs.verify_installed = false
    config.vm.synced_folder './sync', '/vagrant', type: 'rsync', disabled: true

    # Don't inject pub key
    config.ssh.insert_key = false
    config.ssh.username = "vagrant"

    # Loop creating VMs
    servers.each do |machine|
        config.vm.define machine[:hostname] do |node|
            config.vm.box = machine[:box]
            node.vm.hostname = machine[:hostname]

            # Public
            node.vm.network :private_network,
                :ip => machine[:ip]

            node.vm.provider :libvirt do |lv|
                lv.storage_pool_name = params["global"]["pool_name"]
                lv.machine_virtual_size = machine[:disk_size]
                lv.qemu_use_session = false
                lv.cpus = machine[:cpu]
                lv.driver = "kvm"
                lv.memory = machine[:ram]
                lv.nic_model_type = "e1000"
                lv.nested = true
            end
        end
    end

    # Resize net_device's mtu to net_mtu.
    # This is an internal issue due to WireGuard VPN.
    config.vm.provision "shell",
        run: "always",
        inline: "sudo ip link set dev " + params["global"]["net_device"] + " mtu " + params["global"]["net_mtu"]

    # Scripts
    config.vm.provision "shell", path: "scripts/init.sh", privileged: true
    config.vm.provision "shell", path: "scripts/docker.sh", privileged: true
    config.vm.provision "shell", path: "scripts/gitlab_runner.sh", privileged: true
    config.vm.provision "shell", path: "scripts/runner_register.sh", privileged: true, env: {
        "GITLAB_URL"         => params['gitlab']['url'],
        "GITLAB_TOKEN"       => secrets['gitlab']['token'],
        "GITLAB_RUNNER_TYPE" => params['runner']['type']
    }
    config.vm.provision "shell", path: "scripts/addons.sh", privileged: true

    config.trigger.before :destroy do |trigger|
        trigger.name = "Unregistering the runner"
        trigger.run_remote = { inline: "sudo gitlab-runner unregister --url " + params['gitlab']['url'] + " --token " + secrets['gitlab']['token'] }
        trigger.on_error = :halt
    end
end
